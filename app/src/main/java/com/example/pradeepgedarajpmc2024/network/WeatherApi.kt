package com.example.pradeepgedarajpmc2024.network

import com.example.pradeepgedarajpmc2024.models.WeatherPojo
import com.example.pradeepgedarajpmc2024.utils.Constants.API_KEY
import retrofit2.http.GET
import retrofit2.http.Query
import javax.inject.Singleton

@Singleton
interface WeatherApi {
    @GET("data/2.5/weather")
    suspend fun getWeatherByCityName(
        @Query("q") cityName: String="Zocca",
        @Query("appid") apiKey: String = API_KEY
    ): WeatherPojo


    @GET("data/2.5/weather")
    suspend fun getWeatherByCoordinates(
        @Query("lat") latitude: Double,
        @Query("lon") longitude: Double,
        @Query("appid") apiKey: String = API_KEY
    ): WeatherPojo
}