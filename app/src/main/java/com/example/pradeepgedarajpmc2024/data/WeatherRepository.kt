package com.example.pradeepgedarajpmc2024.data

import android.util.Log
import com.example.pradeepgedarajpmc2024.models.WeatherPojo
import com.example.pradeepgedarajpmc2024.network.WeatherApi
import javax.inject.Inject

class WeatherRepository @Inject constructor(private val api: WeatherApi) {

    suspend fun getWeatherByCity(cityName: String): DataOrException<WeatherPojo, Boolean, Exception> {
        return try {
            val response = api.getWeatherByCityName(cityName)
            DataOrException(data = response)
        } catch (e: Exception) {
            Log.d("REX", "getWeatherByCity: $e")
            DataOrException(e = e)
        }
    }

    suspend fun getWeatherByCoordinates(
        latitude: Double,
        longitude: Double
    ): DataOrException<WeatherPojo, Boolean, Exception> {
        return try {
            val response = api.getWeatherByCoordinates(latitude, longitude)
            DataOrException(data = response)
        } catch (e: Exception) {
            Log.d("REX", "getWeatherByCoordinates: $e")
            DataOrException(e = e)
        }
    }
}