package com.example.pradeepgedarajpmc2024.viewmodels

import android.Manifest
import android.app.Application
import android.content.pm.PackageManager
import android.location.Geocoder
import android.location.Location
import android.util.Log
import androidx.core.content.ContextCompat
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.pradeepgedarajpmc2024.data.DataOrException
import com.example.pradeepgedarajpmc2024.data.WeatherRepository
import com.example.pradeepgedarajpmc2024.models.WeatherPojo
import com.google.android.gms.location.FusedLocationProviderClient
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext
import java.util.Locale
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val weatherRepository: WeatherRepository,
    private val fusedLocationProviderClient: FusedLocationProviderClient,
    application: Application
) : AndroidViewModel(application) {

    private var _currentCityName = MutableLiveData<String>()
   // val currentCityName: LiveData<String> get() = _currentCityName
    val currentCityName: LiveData<String> get() = _currentCityName


    suspend fun getWeatherData(city: String)
            : DataOrException<WeatherPojo, Boolean, Exception> {
        return weatherRepository.getWeatherByCity( city)

    }

//    fun setCityName(city: String) {
//        _currentCityName.value = city
//    }

    fun fetchLocationAndSetCityName() {
        viewModelScope.launch {
            try {
                val locationPermissionGranted = checkLocationPermission()
                if (locationPermissionGranted) {
                    // Get the last known location and set the city name
                    val location: Location? = fusedLocationProviderClient.lastLocation.await()
                    location?.let { loc ->
                        val geocoder = Geocoder(getApplication<Application>().applicationContext, Locale.getDefault())
                        val addresses = geocoder.getFromLocation(loc.latitude, loc.longitude, 1)
                        if (addresses != null) {
                            if (addresses.isNotEmpty()) {
                                val cityName = addresses?.get(0)?.locality ?: "Unknown"
                                _currentCityName.postValue(cityName)
                            } else {
                                _currentCityName.postValue("Unknown Location")
                            }
                        }
                    } ?: run {
                        _currentCityName.postValue("Location not available")
                    }
                } else {
                    _currentCityName.postValue("Permission denied")
                }
            } catch (e: Exception) {
                _currentCityName.postValue(e.message ?: "Error getting location")
            }
        }
    }

    private suspend fun checkLocationPermission(): Boolean {
        return withContext(Dispatchers.IO) {
            ContextCompat.checkSelfPermission(
                getApplication<Application>().applicationContext,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        }
    }

}


