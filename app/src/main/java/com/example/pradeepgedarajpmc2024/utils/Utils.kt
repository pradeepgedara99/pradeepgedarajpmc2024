package com.example.pradeepgedarajpmc2024.utils

import java.text.SimpleDateFormat
import kotlin.math.roundToInt

fun formatDate(timestamp: Int): String {
    val sdf = SimpleDateFormat("EEE, MMM d")
    val date = java.util.Date(timestamp.toLong() * 1000)

    return sdf.format(date)
}

fun formatDateTime(timestamp: Int): String {
    val sdf = SimpleDateFormat("hh:mm:aa")
    val date = java.util.Date(timestamp.toLong() * 1000)

    return sdf.format(date)
}

fun formatDecimals(item: Double): String {
    return " %.0f".format(item)
}

fun kelvinToFahrenheit(kelvin: Double): String {
    val fahrenheit = ((kelvin - 273.15) * 9 / 5 + 32).roundToInt()
    return fahrenheit.toString()
}
