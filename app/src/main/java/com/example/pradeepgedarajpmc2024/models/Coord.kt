package com.example.pradeepgedarajpmc2024.models

data class Coord(
    val lat: Double,
    val lon: Double
)