package com.example.pradeepgedarajpmc2024.models

data class Weather(
    val description: String,
    val icon: String,
    val id: Int,
    val main: String
)