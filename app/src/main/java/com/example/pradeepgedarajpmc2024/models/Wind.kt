package com.example.pradeepgedarajpmc2024.models

data class Wind(
    val deg: Int,
    val speed: Double
)