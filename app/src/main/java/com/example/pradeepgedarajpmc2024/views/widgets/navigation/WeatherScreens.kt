package com.example.pradeepgedarajpmc2024.views.widgets.navigation

enum class WeatherScreens {
    SplashScreen,
    MainScreen,
    SearchScreen,
}