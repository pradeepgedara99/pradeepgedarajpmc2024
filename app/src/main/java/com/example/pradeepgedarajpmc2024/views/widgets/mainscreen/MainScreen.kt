package com.example.pradeepgedarajpmc2024.views.widgets.mainscreen

import android.annotation.SuppressLint
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Divider
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.runtime.produceState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import com.example.pradeepgedarachase.widgets.HumidityWinsPressureRow
import com.example.pradeepgedarachase.widgets.SunsetAndSunRiseRow
import com.example.pradeepgedarachase.widgets.WeatherAppBar
import com.example.pradeepgedarachase.widgets.WeatherStateImage
import com.example.pradeepgedarajpmc2024.data.DataOrException
import com.example.pradeepgedarajpmc2024.models.WeatherPojo
import com.example.pradeepgedarajpmc2024.utils.formatDecimals
import com.example.pradeepgedarajpmc2024.utils.kelvinToFahrenheit
import com.example.pradeepgedarajpmc2024.viewmodels.MainViewModel
import com.example.pradeepgedarajpmc2024.views.widgets.navigation.WeatherScreens


@Composable
fun MainScreen(
    navController: NavHostController,
    viewModel: MainViewModel = hiltViewModel(),
    city: String?
) {
    val weatherData = produceState<DataOrException<WeatherPojo, Boolean, Exception>>(
        initialValue = DataOrException(loading = true)
    ) {
        value = city?.let { viewModel.getWeatherData(it) }!!
    }.value


    if (weatherData.loading == true) {
        CircularProgressIndicator()

    } else if (weatherData.data != null) {
        MainScaffold(weatherData.data!!, navController)

    }
}
@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun MainScaffold(weather: WeatherPojo?, navController: NavHostController) {

    Scaffold(topBar = {
        weather?.name?.let {
            WeatherAppBar(
                title = it,
                navController = navController,
                onAddActionClicked = {
                    navController.navigate(WeatherScreens.SearchScreen.name)
                },
                elevation = 5.dp,
                icon = Icons.Default.ArrowBack
            )
        }
    }) {

        MainContent(data = weather)


    }

}

@Composable
fun MainContent(data: WeatherPojo?) {
    val weatherItem = data?.weather?.get(0)?.icon
    val imageUrl = "https://openweathermap.org/img/wn/${
        weatherItem
    }.png"

    Column(
        Modifier
            .padding(4.dp)
            .fillMaxWidth().fillMaxHeight(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
//        Text(
//            text = formatDate(data?.dt),
//            style = MaterialTheme.typography.caption,
//            color = MaterialTheme.colors.onSecondary,
//            fontWeight = FontWeight.SemiBold,
//            modifier = Modifier.padding(6.dp)
//        )
        androidx.compose.material3.Text(text = "WEATEHR",
            modifier = Modifier.align(Alignment.CenterHorizontally))
        Surface(
            modifier = Modifier
                .padding(4.dp)
                .size(200.dp), shape = CircleShape,
            color = Color(0xFFFFC400)
        ) {
            Column(
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                WeatherStateImage(imageUrl = imageUrl)
                if (data != null) {
                    Text(
                        text = kelvinToFahrenheit(data.main.temp)+"F",
                        style = MaterialTheme.typography.h4,
                        fontWeight = FontWeight.ExtraBold
                    )
                }
                data?.name?.let { Text(text = it, fontStyle = FontStyle.Italic) }
            }

        }
        if (data != null) {
            HumidityWinsPressureRow(weather = data.main)
        }
        Divider()
        data?.sys?.let { SunsetAndSunRiseRow(it) }

//        Text(
//            text = "This Week", fontWeight = FontWeight.ExtraBold,
//            style = MaterialTheme.typography.h6,
//            modifier = Modifier.fillMaxWidth(),
//            textAlign = TextAlign.Center
//        )

        Surface(
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight(),
            color = Color(0xFFEEF1EF),
            shape = RoundedCornerShape(size = 14.dp)
        ) {

//            LazyColumn(
//                modifier = Modifier.padding(2.dp),
//                contentPadding = PaddingValues(1.dp),
//                verticalArrangement = Arrangement.spacedBy(8.dp)
//            ) {
//                items(items = data.dt) { item: WeatherItem ->
//
//                    WeatherDetailRow()
//
//                }
//
//            }

        }


    }


}
