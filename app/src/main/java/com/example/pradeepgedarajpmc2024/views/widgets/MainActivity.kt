package com.example.pradeepgedarajpmc2024.views.widgets

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import com.example.pradeepgedarajpmc2024.views.widgets.navigation.WeatherNavigation
import com.example.pradeepgedarajpmc2024.ui.theme.PradeepGedaraJPMC2024Theme
import com.example.pradeepgedarajpmc2024.viewmodels.MainViewModel
import dagger.hilt.android.AndroidEntryPoint
import com.google.android.gms.location.FusedLocationProviderClient



@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val mainViewModel: MainViewModel by viewModels()

        setContent {
            PradeepGedaraJPMC2024Theme {
                // Observe the currentCityName LiveData and collect it as a state
                val currentCityName by mainViewModel.currentCityName.observeAsState()

                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    // Check if the currentCityName is not null or empty to show MyApp
                    if (!currentCityName.isNullOrEmpty()) {
                        MyApp()
                    } else {
                        RequestPermissionUI {
                            mainViewModel.fetchLocationAndSetCityName()
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun RequestPermissionUI(fetchLocationAndSetCityName: () -> Unit) {
    val context = LocalContext.current
    var showDialog by remember { mutableStateOf(false) }

    val launcher = rememberLauncherForActivityResult(
        contract = ActivityResultContracts.RequestPermission(),
        onResult = { isGranted: Boolean ->
            if (isGranted) {
                fetchLocationAndSetCityName()
            } else {
                showDialog = true
            }
        }
    )

    // Request permission when the composable is first put on the screen
    LaunchedEffect(Unit) {
        if (ContextCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            launcher.launch(Manifest.permission.ACCESS_FINE_LOCATION)
        } else {
            fetchLocationAndSetCityName()
        }
    }

    if (showDialog) {
        // This dialog is shown if the user has denied the permission request
        AlertDialog(
            onDismissRequest = { showDialog = false },
            title = { Text("Permission Required") },
            text = { Text("Location permission is required to proceed. Please grant it.") },
            confirmButton = {
                Button(
                    onClick = {
                        showDialog = false
                        launcher.launch(Manifest.permission.ACCESS_FINE_LOCATION)
                    }
                ) {
                    Text("Grant Permission")
                }
            }
        )
    }
}

@Preview(showBackground = true)
@Composable
fun RequestPermissionUIPreview() {
    RequestPermissionUI(fetchLocationAndSetCityName = {})
}



@Composable
fun Greeting(name: String, modifier: Modifier = Modifier) {
    Text(
        text = "Hello $name!",
        modifier = modifier
    )
}

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    PradeepGedaraJPMC2024Theme {
        Greeting("Android")
    }
}

@Composable
fun MyApp() {
    Surface(
        modifier = Modifier.fillMaxSize(),
        color = MaterialTheme.colorScheme.background
    ) {

        Column(
            modifier = Modifier.fillMaxSize(), verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {

            WeatherNavigation()
        }


    }
}