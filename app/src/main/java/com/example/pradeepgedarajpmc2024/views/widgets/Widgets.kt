package com.example.pradeepgedarachase.widgets

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import coil.compose.rememberAsyncImagePainter
import com.example.pradeepgedarajpmc2024.R
import com.example.pradeepgedarajpmc2024.models.Main
import com.example.pradeepgedarajpmc2024.models.Sys
import com.example.pradeepgedarajpmc2024.utils.formatDateTime


@Composable
fun SunsetAndSunRiseRow(weatherItem: Sys) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 15.dp, bottom = 6.dp),
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically
    ) {
        Row() {
            Icon(
                painter = painterResource(id = R.drawable.sunrise), contentDescription = "SunRise",
                modifier = Modifier.size(30.dp)
            )
            Text(
                text = formatDateTime(weatherItem.sunrise),
                style = MaterialTheme.typography.caption
            )

        }
        Row() {

            Text(
                text = formatDateTime(weatherItem.sunset),
                style = MaterialTheme.typography.caption
            )
            Icon(
                painter = painterResource(id = R.drawable.sunset), contentDescription = "SunSet",
                modifier = Modifier.size(30.dp)
            )

        }

    }
}

@Composable
fun HumidityWinsPressureRow(weather: Main) {
    Row(
        modifier = Modifier
            .padding(12.dp)
            .fillMaxWidth(),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.SpaceBetween
    ) {

        Row(modifier = Modifier.padding(4.dp)) {
            Icon(
                painter = painterResource(id = R.drawable.humidity),
                contentDescription = " humidity icon",
                modifier = Modifier.size(20.dp)
            )
            Text(
                text = "${weather.humidity}%",
                style = MaterialTheme.typography.caption
            )

        }
        Row() {

            Icon(
                painter = painterResource(id = R.drawable.pressure),
                contentDescription = " Pressure icon",
                modifier = Modifier.size(20.dp)
            )
            Text(text = "${weather.pressure} psi", style = MaterialTheme.typography.caption)
        }
        Row() {

            Icon(
                painter = painterResource(id = R.drawable.wind), contentDescription = " Wind icon",
                modifier = Modifier.size(20.dp)
            )
            Text(text = "${weather.humidity} mph", style = MaterialTheme.typography.caption)
        }

    }

}


@Composable
fun WeatherStateImage(imageUrl: String) {
    Image(
        painter = rememberAsyncImagePainter(model = imageUrl), contentDescription = "Icon Image",
        modifier = Modifier.size(80.dp)
    )
}

@Preview
@Composable
fun Border(){
    Column(modifier = Modifier.fillMaxWidth()
        .height(200.dp)
        .border(border = BorderStroke(width = 5.dp, color = Color.Blue))) {
        androidx.compose.material3.Text(text = "WEATEHR",
            modifier = Modifier.align(Alignment.CenterHorizontally))
    }

}