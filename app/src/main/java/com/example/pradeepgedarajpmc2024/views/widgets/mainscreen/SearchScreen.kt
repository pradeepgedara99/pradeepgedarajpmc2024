package com.example.pradeepgedarajpmc2024.views.widgets.mainscreen

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.util.Log
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Scaffold
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.core.content.ContextCompat.checkSelfPermission
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import com.example.pradeepgedarachase.widgets.WeatherAppBar
import com.example.pradeepgedarajpmc2024.viewmodels.MainViewModel
import com.example.pradeepgedarajpmc2024.views.widgets.navigation.WeatherScreens


@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun SearchScreen(navController: NavController) {
    Scaffold(topBar = {

        WeatherAppBar(
            title = "Search",
            navController = navController,
            icon = Icons.Default.ArrowBack,
            isMainScreen = false,

            ) {
            navController.popBackStack()
        }

    }) {
        Surface() {
            Column(verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally) {
                SearchBar(modifier = Modifier
                    .fillMaxWidth()
                    .padding(16.dp)
                    .align(Alignment.CenterHorizontally)
                ){mCity->

                    navController.navigate(WeatherScreens.MainScreen.name+ "/$mCity")

                }

            }
        }

    }

}

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun SearchBar (modifier: Modifier,
               onSearch : (String) -> Unit={} ){
    val searchQueryState = rememberSaveable { mutableStateOf("")  }
    val keyboardController = LocalSoftwareKeyboardController.current
    val valid = remember(searchQueryState.value,){
        searchQueryState.value.trim().isNotEmpty()

    }
    Column() {
        CommonTextField(
            valueState = searchQueryState,
            placeHolder = "City Name",
            onAction = KeyboardActions {

                if(!valid){
                    return@KeyboardActions
                }
                onSearch(searchQueryState.value.trim())
                searchQueryState.value = ""
                keyboardController?.hide()
            }

        )
    }
}

@Composable
fun CommonTextField(valueState: MutableState<String>, placeHolder: String,keyboardType: KeyboardType= KeyboardType.Text,
                    imeAction: ImeAction = ImeAction.Next,  onAction: KeyboardActions = KeyboardActions.Default
) {

    OutlinedTextField(value = valueState.value, onValueChange ={valueState.value = it},
        label = { Text(text = placeHolder)},
        maxLines = 1,
        singleLine = true,
        keyboardOptions = KeyboardOptions(keyboardType = keyboardType, imeAction = imeAction),
        keyboardActions = onAction,
        colors = TextFieldDefaults.outlinedTextFieldColors(
            focusedBorderColor = Color.Blue,
            cursorColor = Color.Black
        ),
        shape = RoundedCornerShape(15.dp),
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 10.dp)

    )

}

@Composable
fun SplashScreen(navController: NavHostController,viewModel: MainViewModel = hiltViewModel()) {
    val defaultCity by viewModel.currentCityName.observeAsState()
    if (defaultCity.isNullOrEmpty()){
        navController.navigate(WeatherScreens.MainScreen.name+"/Dallas")    }
    else{
        navController.navigate(WeatherScreens.MainScreen.name+"/${defaultCity}")
    }

}

//@Composable
//fun SplashScreen(navController: NavHostController, viewModel: MainViewModel = hiltViewModel()) {
//    val context = LocalContext.current
//    val defaultCity by viewModel.currentCityName.observeAsState()
//    val hasLocationPermission = checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
//
//    // Request permission result launcher
//    val requestPermissionLauncher = rememberLauncherForActivityResult(
//        contract = ActivityResultContracts.RequestPermission(),
//        onResult = { isGranted ->
//            if (isGranted) {
//                viewModel.fetchLocationAndSetCityName()
//            } else {
//                // Handle the case where permission is denied
//            }
//        }
//    )
//
//    // Side-effect to request permission or fetch location when the composable enters composition
//    LaunchedEffect(key1 = true) {
//        if (hasLocationPermission) {
//            viewModel.fetchLocationAndSetCityName()
//        } else {
//            requestPermissionLauncher.launch(Manifest.permission.ACCESS_FINE_LOCATION)
//        }
//    }
//
//    // Navigate based on the availability of the default city
//    LaunchedEffect(key1 = defaultCity) {
//        val city = defaultCity ?: "San Diego" // Default to San Diego if no city is found
//        navController.navigate(WeatherScreens.MainScreen.name + "/$city")
//    }
//}

